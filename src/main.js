import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import VueTextareaAutosize from 'vue-textarea-autosize';
import firebase from 'firebase/app';
import 'firebase/firestore';
Vue.use(VueTextareaAutosize);

Vue.config.productionTip = false

firebase.initializeApp({
    apiKey: "AIzaSyDZxW8FruSmqNxUX5iUnh18R9mOMCqWmkg",
    authDomain: "vuecalendar-b1e21.firebaseapp.com",
    databaseURL: "https://vuecalendar-b1e21.firebaseio.com",
    projectId: "vuecalendar-b1e21",
    storageBucket: "vuecalendar-b1e21.appspot.com",
    messagingSenderId: "843778380661",
    appId: "1:843778380661:web:ef55d0fb42e97b5e1d793c"
})

export const db = firebase.firestore();

new Vue({
  vuetify,
  render: h => h(App)
}).$mount('#app')
